#!/bin/sh
## 

## 
## Xe1phix.asc - Xe1phix's Public GPG Key
## Xe1phix.key - Xe1phix's Public GPG Key
## Xe1phix.txt - Xe1phix's Public GPG Key
## 
## Xe1phix.asc.sha1   - The SHA1 Hash of Xe1phix's Public GPG Key
## Xe1phix.asc.sha256 - The SHA256 Hash of Xe1phix's Public GPG Key
## Xe1phix.asc.sha512 - The SHA512 Hash of Xe1phix's Public GPG Key
## 
## Xe1phix-GnuPG-Hashsums.txt         - The Hashsums of The Files in The GnuPG Repo
## Xe1phix-GnuPG-Hashsums.txt.asc     - The GPG Signature of Xe1phix-GnuPG-Hashsums.txt
## Xe1phix-GnuPG-Fingerprints.txt     - The GPG Fingerprints of The Files in The GnuPG Repo
## Xe1phix-GnuPG-Fingerprints.txt.asc - The GPG Signature of The Xe1phix-GnuPG-Fingerprints.txt File
## 
## Xe1phixSources-v2.7.list           - Xe1phix's Custom Made sources.list (/etc/apt/sources.list)
## 



## Clone The GnuPG Repo:
git clone https://gitlab.com/xe1phix/Gnupg


## Move Into That Directory:
cd GnuPG

## Import Xe1phix's Public GPG Key:
gpg --verbose --import Xe1phix.asc

## List Xe1phix's GPG Key
gpg --verbose --list-key 8C2731DD2541089E88181251760286DD6EC3F80D

## Print Xe1phix's Fingerprints:
gpg --verbose --keyid-format 0xlong --fingerprint
gpg --verbose --fingerprint --with-subkey-fingerprint
gpg --fingerprint --with-key-origin --with-subkey-fingerprint --with-tofu-info 8C2731DD2541089E88181251760286DD6EC3F80D


## pub   rsa4096 2018-06-28 [SC] [expires: 2019-02-23]
##       8C27 31DD 2541 089E 8818  1251 7602 86DD 6EC3 F80D
## uid           [ultimate] Xe1phix ("From Nothing To Being There is No Logic Bridge.") <Xe1phix@mail.i2p>
## uid           [ultimate] xe1phix (It is No Measure of Health To Be Well Adjusted To A Profoundly Sick Society.) <xe1phix@protonmail.ch>
## sub   rsa4096 2018-06-28 [E] [expires: 2019-02-23]


## Sign Xe1phix's GPG Public Key With Your GPG Private Key:
gpg --verbose --lsign 8C2731DD2541089E88181251760286DD6EC3F80D

## Verify The Integrity of The Xe1phix-GnuPG-Fingerprints.txt File Against Xe1phix's GPG Key:
gpg --verbose --verify Xe1phix-GnuPG-Fingerprints.txt.asc Xe1phix-GnuPG-Fingerprints.txt

## Verify The Integrity of The Xe1phix-GnuPG-Hashsums.txt File Against Xe1phix's GPG Key:
gpg --verbose --verify Xe1phix-GnuPG-Hashsums.txt.asc Xe1phix-GnuPG-Hashsums.txt

## Print The Hashsums of Xe1phix's GPG Key:
openssl dgst -sha1 Xe1phix.asc
openssl dgst -sha256 Xe1phix.asc
openssl dgst -sha512 Xe1phix.asc

## Print The Hashsums of Xe1phix-GnuPG-Fingerprints.txt:
openssl dgst -sha1 Xe1phix-GnuPG-Fingerprints.txt
openssl dgst -sha256 Xe1phix-GnuPG-Fingerprints.txt
openssl dgst -sha512 Xe1phix-GnuPG-Fingerprints.txt

## Print The Hashsums of Xe1phix-GnuPG-Fingerprints.txt.asc:
openssl dgst -sha1 Xe1phix-GnuPG-Fingerprints.txt.asc
openssl dgst -sha256 Xe1phix-GnuPG-Fingerprints.txt.asc
openssl dgst -sha512 Xe1phix-GnuPG-Fingerprints.txt.asc


