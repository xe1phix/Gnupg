## In this video we're going to use gtkhash to generate
## hashsums of the files in my GnuPG Gitlab repo:

Xe1phix.asc
Xe1phix-GnuPG-Fingerprints.txt
Xe1phix-GnuPG-Fingerprints.txt.asc
Xe1phix-GnuPG-Hashsums.txt
Xe1phix-GnuPG-Hashsums.txt.asc
Xe1phixCollection-v8.4.asc
Xe1phixSources-v2.7.list
Xe1phixSources-v2.7.list.asc


openssl dgst -sha1 Xe1phix-GnuPG-Fingerprints.txt > Xe1phix-GnuPG-Fingerprints.txt.sha1
openssl dgst -sha256 Xe1phix-GnuPG-Fingerprints.txt > Xe1phix-GnuPG-Fingerprints.txt.sha256
openssl dgst -sha512 Xe1phix-GnuPG-Fingerprints.txt > Xe1phix-GnuPG-Fingerprints.txt.sha512

openssl dgst -sha1 Xe1phix-GnuPG-Fingerprints.txt.asc > Xe1phix-GnuPG-Fingerprints.txt.asc.sha1
openssl dgst -sha256 Xe1phix-GnuPG-Fingerprints.txt.asc > Xe1phix-GnuPG-Fingerprints.txt.asc.sha256
openssl dgst -sha512 Xe1phix-GnuPG-Fingerprints.txt.asc > Xe1phix-GnuPG-Fingerprints.txt.asc.sha512

openssl dgst -sha1 Xe1phix-GnuPG-Hashsums.txt > Xe1phix-GnuPG-Hashsums.txt.sha1
openssl dgst -sha256 Xe1phix-GnuPG-Hashsums.txt > Xe1phix-GnuPG-Hashsums.txt.sha256
openssl dgst -sha512 Xe1phix-GnuPG-Hashsums.txt > Xe1phix-GnuPG-Hashsums.txt.sha512

openssl dgst -sha1 Xe1phix-GnuPG-Hashsums.txt.asc > Xe1phix-GnuPG-Hashsums.txt.asc.sha1
openssl dgst -sha256 Xe1phix-GnuPG-Hashsums.txt.asc > Xe1phix-GnuPG-Hashsums.txt.asc.sha256
openssl dgst -sha512 Xe1phix-GnuPG-Hashsums.txt.asc > Xe1phix-GnuPG-Hashsums.txt.asc.sha512

openssl dgst -sha1 Xe1phixCollection-v8.4.asc > Xe1phixCollection-v8.4.asc.sha1
openssl dgst -sha256 Xe1phixCollection-v8.4.asc > Xe1phixCollection-v8.4.asc.sha256
openssl dgst -sha512 Xe1phixCollection-v8.4.asc > Xe1phixCollection-v8.4.asc.sha512

openssl dgst -sha1 Xe1phixSources-v2.7.list > Xe1phixSources-v2.7.list.sha1
openssl dgst -sha256 Xe1phixSources-v2.7.list > Xe1phixSources-v2.7.list.sha256
openssl dgst -sha512 Xe1phixSources-v2.7.list > Xe1phixSources-v2.7.list.sha512

openssl dgst -sha1 Xe1phixSources-v2.7.list.asc > Xe1phixSources-v2.7.list.asc.sha1
openssl dgst -sha256 Xe1phixSources-v2.7.list.asc > Xe1phixSources-v2.7.list.asc.sha256
openssl dgst -sha512 Xe1phixSources-v2.7.list.asc > Xe1phixSources-v2.7.list.asc.sha512

openssl dgst -sha1 0x8C2731DD2541089E88181251760286DD6EC3F80D.asc > 0x8C2731DD2541089E88181251760286DD6EC3F80D.asc.sha1
openssl dgst -sha256 0x8C2731DD2541089E88181251760286DD6EC3F80D.asc > 0x8C2731DD2541089E88181251760286DD6EC3F80D.asc.sha256
openssl dgst -sha512 0x8C2731DD2541089E88181251760286DD6EC3F80D.asc > 0x8C2731DD2541089E88181251760286DD6EC3F80D.asc.sha512




openssl dgst -sha1 > .sha1
openssl dgst -sha256 > .sha256
openssl dgst -sha512  > .sha512

openssl dgst -sha1 > .sha1
openssl dgst -sha256 > .sha256
openssl dgst -sha512  > .sha512


